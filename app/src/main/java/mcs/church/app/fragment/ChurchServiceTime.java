package mcs.church.app.fragment;

import android.support.v4.app.Fragment;
import android.widget.LinearLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import mcs.church.app.R;
import mcs.church.app.model.Church;
import mcs.church.app.view.ServiceTime;
import mcs.church.app.view.ServiceTime_;

/**
 * Created by h3r0 on 5/5/14.
 */
@EFragment(R.layout.fragment_church_servicetime)
public class ChurchServiceTime extends Fragment {

    Church church;

    @ViewById
    LinearLayout main;

    public void setChurch(Church church) {
        this.church = church;
    }

    @AfterViews
    void AfterView() {

        if(!church.service_times.monday.isEmpty()){
            ServiceTime time = ServiceTime_.build(getActivity().getApplicationContext());
            time.bind(church.service_times.monday);
            main.addView(time);
        }

        if(!church.service_times.tuesday.isEmpty()){
            ServiceTime time = ServiceTime_.build(getActivity().getApplicationContext());
            time.bind(church.service_times.tuesday);
            main.addView(time);
        }

        if(!church.service_times.wednesday.isEmpty()){
            ServiceTime time = ServiceTime_.build(getActivity().getApplicationContext());
            time.bind(church.service_times.wednesday);
            main.addView(time);
        }

        if(!church.service_times.thursday.isEmpty()){
            ServiceTime time = ServiceTime_.build(getActivity().getApplicationContext());
            time.bind(church.service_times.thursday);
            main.addView(time);
        }

        if(!church.service_times.friday.isEmpty()){
            ServiceTime time = ServiceTime_.build(getActivity().getApplicationContext());
            time.bind(church.service_times.friday);
            main.addView(time);
        }

        if(!church.service_times.saturday.isEmpty()){
            ServiceTime time = ServiceTime_.build(getActivity().getApplicationContext());
            time.bind(church.service_times.saturday);
            main.addView(time);
        }

        if(!church.service_times.sunday.isEmpty()){
            ServiceTime time = ServiceTime_.build(getActivity().getApplicationContext());
            time.bind(church.service_times.sunday);
            main.addView(time);
        }

   }

}