package mcs.church.app.fragment;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import mcs.church.app.R;
import mcs.church.app.model.ChurchMessage;
import mcs.church.app.util.L;

/**
 * Created by h3r0 on 5/14/14.
 */
@EFragment(R.layout.fragment_message)
public class MessageTitle extends Fragment {

    public void setObject(ChurchMessage object) {
        this.object = object;
    }

    ChurchMessage object;

    @ViewById
    ImageView background;

    @ViewById
    ImageView gospel_message;

    @ViewById
    TextView error_message;

    @ViewById
    ProgressBar progressBar;

    public MessageTitle() {
    }

    @AfterViews
    void AfterViews() {

        background.setImageResource(object.getBackgroundImage());

        gospel_message.setVisibility(ImageView.GONE);
        error_message.setVisibility(TextView.GONE);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        if((object.id % 2 == 0) && object.id != 8){
            gospel_message.setBackgroundColor(Color.argb(122,0,0,0));
        }

        Picasso.with(getActivity().getApplicationContext())
                .load(object.title_image)
                .into(gospel_message, new Callback() {
                    @Override
                    public void onSuccess() {

                        if(getActivity() != null){
                            if ((object.id % 2) != 0){
                                Animation anim = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),object.getAnimationType());
                                gospel_message.startAnimation(anim);
                                gospel_message.setVisibility(ImageView.VISIBLE);
                                progressBar.setVisibility(ProgressBar.GONE);
                            }else{
                                gospel_message.setVisibility(ImageView.VISIBLE);
                                progressBar.setVisibility(ProgressBar.GONE);
                            }
                        }

                    }

                    @Override
                    public void onError() {
                        gospel_message.setVisibility(ImageView.GONE);
                        progressBar.setVisibility(ProgressBar.GONE);
                        error_message.setVisibility(TextView.VISIBLE);
                    }
                });

    }

}
