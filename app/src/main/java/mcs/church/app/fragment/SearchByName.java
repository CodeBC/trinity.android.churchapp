package mcs.church.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.api.BackgroundExecutor;

import java.util.ArrayList;

import mcs.church.app.R;
import mcs.church.app.adapter.DirectoryAdapter;
import mcs.church.app.model.Church;
import mcs.church.app.ui.DirectoryDetail_;
import mcs.church.app.util.Constants;
import mcs.church.app.util.Utils;
import mcs.church.app.view.AnimatedRectLayout;
import mcs.church.app.view.FloatLabeledEditText;

/**
 * Created by h3r0 on 5/16/14.
 */
@EFragment(R.layout.fragment_search_name)
public class SearchByName extends Fragment{

    @Bean
    Utils utils;

    @ViewById
    FloatLabeledEditText search_text;

    @ViewById
    FrameLayout content;

    @ViewById
    ListView list_directory;

    @ViewById
    ProgressBar progressBar;

    @ViewById
    TextView error_message;

    @Bean
    DirectoryAdapter adapter;

    String search_index = "";

    ArrayList<Church> objects = new ArrayList<Church>();
    ArrayList<Church> search_result = new ArrayList<Church>();

    public SearchByName() {
    }

    @AfterViews
    void AfterViews(){

        objects = (ArrayList<Church>) utils.ReadArrayListFromInternal(Constants.file_directory);
        adapter.setData(search_result);

        list_directory.setAdapter(adapter);

        search_text.getEditText().addTextChangedListener(onTextChanged);
    }

    private TextWatcher onTextChanged = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(editable.length() != 0 ){
                content.setVisibility(FrameLayout.VISIBLE);

                search_index = editable.toString();

                BackgroundExecutor.cancelAll("SearchByName",true);

                UpdateList();

            }else{
                content.setVisibility(FrameLayout.GONE);

                BackgroundExecutor.cancelAll("SearchByName",true);
            }
        }
    };

    @UiThread
    void hide(){
        error_message.setVisibility(TextView.GONE);
        list_directory.setVisibility(ListView.GONE);
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @UiThread
    void show(){
        adapter.setData(search_result);

        if(search_result.isEmpty()){
            error_message.setText("Sorry. There is no church with named '" + search_index + "'");
            error_message.setVisibility(TextView.VISIBLE);
            list_directory.setVisibility(ListView.GONE);
            progressBar.setVisibility(ProgressBar.GONE);
        }else{
            list_directory.setVisibility(ListView.VISIBLE);
            progressBar.setVisibility(ProgressBar.GONE);
        }
    }

    @Background(id = "SearchByName")
    void UpdateList(){

        hide();

        search_result.clear();

        for(int i=0;i<objects.size();i++){
            if(objects.get(i).name.toUpperCase().contains(search_index.toUpperCase())){
                search_result.add(objects.get(i));
            }
        }

        show();
    }

    @ItemClick(R.id.list_directory)
    void GoToDetail(int position) {
        Intent detail = new Intent(getActivity(), DirectoryDetail_.class);
        Bundle args = new Bundle();
        args.putSerializable("church", adapter.getItem(position));
        detail.putExtras(args);
        detail.putExtra("animation_type", AnimatedRectLayout.ANIMATION_RANDOM);
        startActivity(detail);
        getActivity().overridePendingTransition(0,0);
    }
}
