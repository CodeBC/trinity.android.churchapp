package mcs.church.app.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;
import org.androidannotations.api.BackgroundExecutor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;

import mcs.church.app.R;
import mcs.church.app.adapter.DirectoryAdapter;
import mcs.church.app.model.Church;
import mcs.church.app.ui.DirectoryDetail_;
import mcs.church.app.util.Constants;
import mcs.church.app.util.L;
import mcs.church.app.util.Utils;
import mcs.church.app.view.AnimatedRectLayout;
import mcs.church.app.view.RadialPickerLayout;
import mcs.church.app.view.TimePickerDialog;

/**
 * Created by h3r0 on 5/16/14.
 */
@EFragment(R.layout.fragment_search_servicetime)
public class SearchByServiceTiming extends Fragment implements TimePickerDialog.OnTimeSetListener {

    @Bean
    Utils utils;

    @ViewById
    ImageView ic_date;

    @ViewById
    ImageView ic_time;

    @ViewById
    ImageView ic_clear;

    @ViewById
    TextView lbl_datetime;

    @ViewById
    FrameLayout content;

    @ViewById
    ListView list_directory;

    @ViewById
    ProgressBar progressBar;

    @ViewById
    TextView error_message;

    @Bean
    DirectoryAdapter adapter;

    @StringArrayRes
    String[] array_date;

    String search_day = "";
    String search_time = "";
    int hourOfDay = 0;

    ArrayList<Church> objects = new ArrayList<Church>();
    ArrayList<Church> search_result = new ArrayList<Church>();

    TimePickerDialog timePickerDialog;
    Calendar calendar;

    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    ImageView view = (ImageView) v;
                    view.getDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                    view.invalidate();
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    ImageView view = (ImageView) v;
                    view.getDrawable().clearColorFilter();
                    view.invalidate();
                    break;
                }
                case MotionEvent.ACTION_CANCEL: {
                    ImageView view = (ImageView) v;
                    view.getDrawable().clearColorFilter();
                    view.invalidate();
                    break;
                }
            }

            return false;
        }
    };

    public SearchByServiceTiming() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        calendar = Calendar.getInstance();

        timePickerDialog = TimePickerDialog.newInstance(this, calendar.get(Calendar.HOUR_OF_DAY), 0, false, false);
    }

    @AfterViews
    void AfterViews() {
        ic_date.setOnTouchListener(touchListener);
        ic_time.setOnTouchListener(touchListener);
        ic_clear.setOnTouchListener(touchListener);

        objects = (ArrayList<Church>) utils.ReadArrayListFromInternal(Constants.file_directory);
        adapter.setData(search_result);

        list_directory.setAdapter(adapter);

        UpdateText();
    }

    @Click(R.id.ic_date)
    void ChooseDate() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose Date")
                .setItems(R.array.array_date, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        search_day = array_date[which].toString();
                        if (!search_day.equalsIgnoreCase("Any day")) search_time = "";
                        UpdateText();
                    }
                });

        builder.create().show();
    }

    @Click(R.id.lbl_datetime)
    void setDateTime() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose Date")
                .setItems(R.array.array_date, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        search_day = array_date[which].toString();

                        timePickerDialog.setVibrate(true);
                        timePickerDialog.setCloseOnSingleTapMinute(false);
                        timePickerDialog.show(getChildFragmentManager(), "TIMEPICKERTAG");
                    }
                });

        builder.create().show();
    }

    @Click(R.id.ic_time)
    void ChooseTime() {
        timePickerDialog.setVibrate(true);
        timePickerDialog.setCloseOnSingleTapMinute(false);
        timePickerDialog.show(getChildFragmentManager(), "TIMEPICKERTAG");
    }

    @Click(R.id.ic_clear)
    void ClearTime() {
        search_time = "";
        UpdateText();
    }


    void UpdateText() {

        L.i(search_day);
        L.i(search_time);

        if (search_day.isEmpty() && search_time.isEmpty()) {
            BackgroundExecutor.cancelAll("SearchByTime", true);
            return;
        }

        if (search_day.equalsIgnoreCase("Any day") && search_time.isEmpty()) {
            lbl_datetime.setText(search_day);
            BackgroundExecutor.cancelAll("SearchByTime", true);

            search_result.clear();
            search_result.addAll(objects);

            show();

            return;
        }

        if (search_time.isEmpty() && !search_day.isEmpty()) {
            lbl_datetime.setText(search_day);

            BackgroundExecutor.cancelAll("SearchByTime", true);
            UpdateList();

            return;
        }

        if (search_day.isEmpty() && !search_time.isEmpty()) {
            lbl_datetime.setText(search_time);

            BackgroundExecutor.cancelAll("SearchByTime", true);
            UpdateList();

            return;
        }

        if (!search_day.isEmpty() && !search_time.isEmpty()) {
            lbl_datetime.setText(search_day + ", " + search_time);

            BackgroundExecutor.cancelAll("SearchByTime", true);
            UpdateList();
        }
    }

    @UiThread
    void hide() {
        error_message.setVisibility(TextView.GONE);
        list_directory.setVisibility(ListView.GONE);
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @UiThread
    void show() {
        adapter.setData(search_result);

        if (search_result.isEmpty()) {
            if (!search_day.isEmpty() && !search_time.isEmpty())
                error_message.setText("Sorry. There is no church which has '" + search_day + ", " + search_time + "' as service time");
            else if (!search_day.isEmpty() && search_time.isEmpty())
                error_message.setText("Sorry. There is no church which has '" + search_day + "' as service time");
            else if (search_day.isEmpty() && !search_time.isEmpty())
                error_message.setText("Sorry. There is no church which has '" + search_time + "' as service time");
            else
                error_message.setText("Sorry. There is no church which such service time");

            error_message.setVisibility(TextView.VISIBLE);
            list_directory.setVisibility(ListView.GONE);
            progressBar.setVisibility(ProgressBar.GONE);
        } else {
            list_directory.setVisibility(ListView.VISIBLE);
            progressBar.setVisibility(ProgressBar.GONE);
        }
    }

    @Background(id = "SearchByTime")
    void UpdateList() {

        hide();

        search_result.clear();

        if (!search_day.isEmpty() && !search_time.isEmpty()) {
            if (search_day.equalsIgnoreCase("Any day"))
                SearchByTime();
            else
                SearchByBoth();
        } else if (!search_day.isEmpty() && search_time.isEmpty()) {
            SearchByDay();
        } else if (!search_time.isEmpty() && search_day.isEmpty()) {
            SearchByTime();
        }

        HashSet hs = new HashSet();
        hs.addAll(search_result);
        search_result.clear();
        search_result.addAll(hs);

        for (int i = 0; i < search_result.size(); i++) {
            L.i(search_result.get(i).name);
        }

        show();
    }

    void SearchByDay() {

        for (int i = 0; i < objects.size(); i++) {
            if (!objects.get(i).getServiceDaysByDay(search_day).isEmpty()) {
                search_result.add(objects.get(i));
            }
        }
    }

    void SearchByTime() {
        for (int i = 0; i < objects.size(); i++) {
            for (int j = 0; j < objects.get(i).service_times.getAllServiceDays().size(); j++) {
                if (objects.get(i).service_times.getAllServiceDays().get(j).hasTime(search_time)) {
                    search_result.add(objects.get(i));
                }
            }
        }
    }

    void SearchByBoth() {
        for (int i = 0; i < objects.size(); i++) {
            for (int j = 0; j < objects.get(i).service_times.getAllServiceDays().size(); j++) {
                if (objects.get(i).service_times.getAllServiceDays().get(j).hasTime(search_time) && objects.get(i).service_times.getAllServiceDays().get(j).hasDay(search_day)) {
                    search_result.add(objects.get(i));
                }
            }
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {

        if (hourOfDay > 12) {
            search_time = (hourOfDay % 12) + ":00 PM";
        } else if (hourOfDay == 12) {
            search_time = "12:00 PM";
        } else if (hourOfDay < 12) {
            search_time = hourOfDay + ":00 AM";
        }

        this.hourOfDay = hourOfDay;

        UpdateText();
    }

    @ItemClick(R.id.list_directory)
    void GoToDetail(int position) {
        Intent detail = new Intent(getActivity(), DirectoryDetail_.class);
        Bundle args = new Bundle();
        args.putSerializable("church", adapter.getItem(position));
        detail.putExtras(args);
        detail.putExtra("animation_type", AnimatedRectLayout.ANIMATION_RANDOM);
        startActivity(detail);
        getActivity().overridePendingTransition(0, 0);
    }
}
