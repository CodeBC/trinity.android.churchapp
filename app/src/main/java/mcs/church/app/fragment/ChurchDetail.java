package mcs.church.app.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import mcs.church.app.R;
import mcs.church.app.model.Church;

/**
 * Created by h3r0 on 5/5/14.
 */
@EFragment(R.layout.fragment_church_detail)
public class ChurchDetail extends Fragment {
    @ViewById
    TextView txtLocation;

    @ViewById
    TextView txtPhone;

    @ViewById
    TextView txtEmail;

    Church object;

    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    TextView view = (TextView) v;
                    view.setTextColor(Color.BLUE);
                    view.invalidate();
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    TextView view = (TextView) v;
                    view.setTextColor(Color.BLACK);
                    view.invalidate();
                    break;
                }
                case MotionEvent.ACTION_CANCEL: {
                    TextView view = (TextView) v;
                    view.setTextColor(Color.BLACK);
                    view.invalidate();
                    break;
                }
            }

            return false;
        }
    };

    public void setChurch(Church church) {
        this.object = church;
    }

    @AfterViews
    void AfterView() {

        txtLocation.setText(Html.fromHtml(object.address));
        txtEmail.setText(object.email);
        txtPhone.setText(object.telephone);

        txtLocation.setOnTouchListener(touchListener);
        txtEmail.setOnTouchListener(touchListener);
        txtPhone.setOnTouchListener(touchListener);
    }

    @Click(R.id.txtPhone)
    void Call() {
        String uri = "tel:" + object.telephone;
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

    @Click(R.id.txtEmail)
    void Email() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", object.email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "About " + object.name);
        startActivity(Intent.createChooser(emailIntent, "Send email..."));

    }

    @Click(R.id.txtLocation)
    void Map() {
        double latitude = object.latitude;
        double longitude = object.longitude;
        String label = object.name + ", Address:" + object.address;
        String uriBegin = "geo:" + latitude + "," + longitude;
        String query = latitude + "," + longitude + "(" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=12";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
