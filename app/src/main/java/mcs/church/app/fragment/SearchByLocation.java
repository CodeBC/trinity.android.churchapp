package mcs.church.app.fragment;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import mcs.church.app.R;
import mcs.church.app.adapter.DirectoryAdapter;
import mcs.church.app.model.Church;
import mcs.church.app.model.ServiceTime;
import mcs.church.app.ui.DirectoryDetail_;
import mcs.church.app.util.L;
import mcs.church.app.util.Utils;
import mcs.church.app.view.AnimatedRectLayout;

/**
 * Created by h3r0 on 5/16/14.
 */
@EFragment(R.layout.fragment_search_location)
public class SearchByLocation extends Fragment implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener, LocationListener {

    @Bean
    Utils utils;

    @ViewById
    ListView list_directory;

    @ViewById
    ProgressBar progressBar;

    @ViewById
    TextView error_message;

    @Bean
    DirectoryAdapter adapter;

    LocationClient mLocationClient;

    Location mCurrentLocation;

    LocationRequest mLocationRequest;

    ArrayList<Church> search_result = new ArrayList<Church>();

    public SearchByLocation() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLocationClient = new LocationClient(getActivity(), this, this);
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000 * 60);
        mLocationRequest.setFastestInterval(1000 * 30);
        mLocationRequest.setSmallestDisplacement(1000);
    }

    @AfterViews
    void AfterViews() {
        adapter.setData(search_result);

        list_directory.setAdapter(adapter);

    }

    @Override
    public void onStart() {
        super.onStart();
        mLocationClient.connect();
    }

    @Override
    public void onStop() {
        if (mLocationClient.isConnected()) {
            mLocationClient.removeLocationUpdates(this);
        }
        mLocationClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mCurrentLocation = mLocationClient.getLastLocation();
        mLocationClient.requestLocationUpdates(mLocationRequest, this);

        if (mCurrentLocation != null) {
            L.i("Lat : Lng - " + mCurrentLocation.getLatitude() + " :" + mCurrentLocation.getLongitude());

            UpdateList();
        }
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        L.i("Lat : Lng - " + location.getLatitude() + " :" + location.getLongitude());

        mCurrentLocation = location;

        UpdateList();
    }

    void hide() {
        error_message.setVisibility(TextView.GONE);
        list_directory.setVisibility(ListView.GONE);
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    void show() {
        adapter.setData(search_result);

        if (search_result.isEmpty()) {
            error_message.setText("Sorry. There is no church in this area");
            error_message.setVisibility(TextView.VISIBLE);
            list_directory.setVisibility(ListView.GONE);
            progressBar.setVisibility(ProgressBar.GONE);
        } else {
            list_directory.setVisibility(ListView.VISIBLE);
            progressBar.setVisibility(ProgressBar.GONE);
        }
    }

    void UpdateList() {

        hide();

        String url = "http://mschurch.herokuapp.com/api/churches/nearbys";

        Ion.with(getActivity().getApplicationContext()).load(url)
                .setLogging("Search", Log.DEBUG)
                .setBodyParameter("latitude", mCurrentLocation.getLatitude() + "")
                .setBodyParameter("longitude", mCurrentLocation.getLongitude() + "")
                //.setBodyParameter("latitude", 1.3268938 + "")
                //.setBodyParameter("longitude", 103.7247749 + "")
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {

                        try {
                            if (e != null) throw e;

                            switch (result.getHeaders().getResponseCode()) {
                                case 200:

                                    JsonParser parser = new JsonParser();

                                    InputStream in = new ByteArrayInputStream(result.getResult().getBytes("UTF-8"));

                                    JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
                                    reader.setLenient(true);

                                    JsonArray jarr = (JsonArray) parser.parse(reader);

                                    Gson gson = new GsonBuilder().create();

                                    search_result.clear();

                                    for (int i = 0; i < jarr.size(); i++) {
                                        JsonObject jobj = jarr.get(i).getAsJsonObject();

                                        Church obj = gson.fromJson(jobj, Church.class);
                                        obj.telephone = jobj.get("phone").getAsString();

                                        obj.service_times = new ServiceTime();

                                        for(int j=0;j<obj.service_timings.size();j++){
                                            if(obj.service_timings.get(j).day.equalsIgnoreCase("sunday"))
                                                obj.service_times.sunday.add(obj.service_timings.get(j));

                                            if(obj.service_timings.get(j).day.equalsIgnoreCase("monday"))
                                                obj.service_times.monday.add(obj.service_timings.get(j));

                                            if(obj.service_timings.get(j).day.equalsIgnoreCase("tuesday"))
                                                obj.service_times.tuesday.add(obj.service_timings.get(j));

                                            if(obj.service_timings.get(j).day.equalsIgnoreCase("wednesday"))
                                                obj.service_times.wednesday.add(obj.service_timings.get(j));

                                            if(obj.service_timings.get(j).day.equalsIgnoreCase("thursday"))
                                                obj.service_times.thursday.add(obj.service_timings.get(j));

                                            if(obj.service_timings.get(j).day.equalsIgnoreCase("friday"))
                                                obj.service_times.friday.add(obj.service_timings.get(j));

                                            if(obj.service_timings.get(j).day.equalsIgnoreCase("saturday"))
                                                obj.service_times.saturday.add(obj.service_timings.get(j));
                                        }

                                        search_result.add(obj);
                                    }

                                    Collections.sort(search_result, new Comparator<Church>() {
                                        @Override
                                        public int compare(Church lhs, Church rhs) {
                                            return Double.compare(lhs.distance,rhs.distance);
                                        }
                                    });

                                    adapter.setData(search_result,false);

                                    if (search_result.isEmpty()) {
                                        error_message.setText("Sorry. There is no church in this area");
                                        error_message.setVisibility(TextView.VISIBLE);
                                        list_directory.setVisibility(ListView.GONE);
                                        progressBar.setVisibility(ProgressBar.GONE);
                                    } else {
                                        list_directory.setVisibility(ListView.VISIBLE);
                                        progressBar.setVisibility(ProgressBar.GONE);
                                    }

                                    break;
                                case 204:

                                    error_message.setText("Sorry. There is no church in this area");
                                    error_message.setVisibility(TextView.VISIBLE);
                                    list_directory.setVisibility(ListView.GONE);
                                    progressBar.setVisibility(ProgressBar.GONE);

                                    break;
                                default:

                                    error_message.setText("Unable to connect.");
                                    error_message.setVisibility(TextView.VISIBLE);
                                    list_directory.setVisibility(ListView.GONE);
                                    progressBar.setVisibility(ProgressBar.GONE);

                                    break;
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();

                            error_message.setText("Unable to connect.");
                            error_message.setVisibility(TextView.VISIBLE);
                            list_directory.setVisibility(ListView.GONE);
                            progressBar.setVisibility(ProgressBar.GONE);
                        }
                    }
                });

    }

    @ItemClick(R.id.list_directory)
    void GoToDetail(int position) {
        Intent detail = new Intent(getActivity(), DirectoryDetail_.class);
        Bundle args = new Bundle();
        args.putSerializable("church", adapter.getItem(position));
        detail.putExtras(args);
        detail.putExtra("animation_type", AnimatedRectLayout.ANIMATION_RANDOM);
        startActivity(detail);
        getActivity().overridePendingTransition(0, 0);
    }
}
