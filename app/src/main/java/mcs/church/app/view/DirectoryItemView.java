package mcs.church.app.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.Locale;

import mcs.church.app.R;
import mcs.church.app.model.Church;

/**
 * Created by h3r0 on 4/24/14.
 */
@EViewGroup(R.layout.item_directory)
public class DirectoryItemView extends LinearLayout {

    @ViewById
    TextView name;

    @ViewById
    TextView address;

    @ViewById
    ImageView phone;

    @ViewById
    ImageView map;

    @ViewById
    ImageView mail;

    Church object;

    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    ImageView view = (ImageView) v;
                    view.getDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                    view.invalidate();
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    ImageView view = (ImageView) v;
                    view.getDrawable().clearColorFilter();
                    view.invalidate();
                    break;
                }
                case MotionEvent.ACTION_CANCEL: {
                    ImageView view = (ImageView) v;
                    view.getDrawable().clearColorFilter();
                    view.invalidate();
                    break;
                }
            }

            return false;
        }
    };

    public DirectoryItemView(Context context) {
        super(context);
    }

    public void bind(Church object) {

        this.object = object;

        name.setText(object.name);
        address.setText(Html.fromHtml(object.address));

        phone.setOnTouchListener(touchListener);
        mail.setOnTouchListener(touchListener);
        map.setOnTouchListener(touchListener);

        phone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Call();
            }
        });

        mail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Email();
            }
        });

        map.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Map();
            }
        });
    }

    void Call(){
        String uri = "tel:" + object.telephone;
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        getContext().startActivity(intent);
    }

    void Email(){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",object.email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "About " + object.name);
        getContext().startActivity(Intent.createChooser(emailIntent, "Send email..."));

    }

    void Map(){

        double latitude = object.latitude;
        double longitude = object.longitude;
        String label = object.name + ", Address:" + object.address;
        String uriBegin = "geo:" + latitude + "," + longitude;
        String query = latitude + "," + longitude + "(" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=12";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        getContext().startActivity(intent);
    }

}
