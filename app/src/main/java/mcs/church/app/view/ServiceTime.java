package mcs.church.app.view;

import android.content.Context;
import android.text.Html;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import mcs.church.app.R;
import mcs.church.app.model.ServiceDay;

/**
 * Created by h3r0 on 5/6/14.
 */

@EViewGroup(R.layout.item_servicetime)
public class ServiceTime extends LinearLayout{

    ArrayList<ServiceDay> object;

    @ViewById
    TextView date;

    @ViewById
    TextView time;

    public ServiceTime(Context context) {
        super(context);
    }

    public void bind(ArrayList<ServiceDay> object) {
        this.object = object;

        date.setTextColor(object.get(0).getColor());

        date.setText(object.get(0).day);

        String service_time = "";

        for(int i=0;i<object.size();i++){

            String start = object.get(i).start_hour + ":" + object.get(i).start_minute + " " + object.get(i).start_am_pm;

            service_time += "<p>" + start + "</p>";
        }

        time.setText(Html.fromHtml(service_time));
    }

}
