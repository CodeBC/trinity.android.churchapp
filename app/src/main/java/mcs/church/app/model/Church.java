package mcs.church.app.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by h3r0 on 4/24/14.
 */
public class Church implements Serializable {

    public int id;

    public String name;

    public String address;

    public String telephone;

    public String email;

    public String created_at;

    public String updated_at;

    public double latitude;

    public double longitude;

    public double distance;

    public ServiceTime service_times;

    public ArrayList<ServiceDay> service_timings;

    public ArrayList<ServiceDay> getServiceDaysByDay(String day) {

        if (day.equalsIgnoreCase("sunday")) return service_times.sunday;
        if (day.equalsIgnoreCase("monday")) return service_times.monday;
        if (day.equalsIgnoreCase("tuesday")) return service_times.tuesday;
        if (day.equalsIgnoreCase("wednesday")) return service_times.wednesday;
        if (day.equalsIgnoreCase("thursday")) return service_times.thursday;
        if (day.equalsIgnoreCase("friday")) return service_times.friday;
        if (day.equalsIgnoreCase("saturday")) return service_times.saturday;

        return new ArrayList<ServiceDay>();
    }

}
