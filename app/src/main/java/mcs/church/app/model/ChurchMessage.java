package mcs.church.app.model;

import java.io.Serializable;

import mcs.church.app.R;

/**
 * Created by h3r0 on 5/14/14.
 */
public class ChurchMessage implements Serializable {

    public int id;

    public int order_number;

    public String title;

    public String title_image;

    public String detail_image;

    public String title_image_android;

    public String detail_image_android;

    public int animation_type;

    public String detail_link;

    public int getBackgroundImage() {
        switch (animation_type) {
            case 1:
                return R.drawable.message01_bg;
            case 2:
                return R.drawable.message02_bg;
            case 3:
                return R.drawable.message03_bg;
            case 4:
                return R.drawable.message04_bg;
            case 5:
                return R.drawable.message05_bg;

        }
        return R.drawable.message01_bg;
    }

    public int getAnimationType() {
        switch (animation_type) {
            case 1:
                return R.anim.anim_type_one;
            case 2:
                return R.anim.anim_type_two;
            case 3:
                return R.anim.anim_type_three;
            case 4:
                return R.anim.anim_type_four;
            case 5:
                return R.anim.anim_type_five;
        }
        return R.anim.rotate;
    }
}
