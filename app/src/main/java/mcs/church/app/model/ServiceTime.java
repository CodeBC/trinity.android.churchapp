package mcs.church.app.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by h3r0 on 5/14/14.
 */
public class ServiceTime implements Serializable {

    public ArrayList<ServiceDay> monday = new ArrayList<ServiceDay>();

    public ArrayList<ServiceDay> tuesday = new ArrayList<ServiceDay>();

    public ArrayList<ServiceDay> wednesday = new ArrayList<ServiceDay>();

    public ArrayList<ServiceDay> thursday = new ArrayList<ServiceDay>();

    public ArrayList<ServiceDay> friday = new ArrayList<ServiceDay>();

    public ArrayList<ServiceDay> saturday = new ArrayList<ServiceDay>();

    public ArrayList<ServiceDay> sunday = new ArrayList<ServiceDay>();

    public ArrayList<ServiceDay> getAllServiceDays(){
        ArrayList<ServiceDay> all = new ArrayList<ServiceDay>();

        all.addAll(sunday);
        all.addAll(monday);
        all.addAll(tuesday);
        all.addAll(wednesday);
        all.addAll(thursday);
        all.addAll(friday);
        all.addAll(saturday);

        return all;
    }
}
