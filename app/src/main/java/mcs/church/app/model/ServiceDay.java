package mcs.church.app.model;

import android.graphics.Color;

import java.io.Serializable;

/**
 * Created by h3r0 on 5/14/14.
 */
public class ServiceDay implements Serializable{

    public int church_id;

    public String created_at;

    public String day;

    public String end_am_pm;

    public String end_hour;

    public String end_minute;

    public int id;

    public String start_am_pm;

    public String start_hour;

    public String start_minute;

    public String updated_at;

    public int getColor(){
        if(day.equalsIgnoreCase("saturday")) return Color.RED;
        if(day.equalsIgnoreCase("sunday")) return Color.RED;
        return Color.BLACK;
    }

    public boolean hasDay(String day){
        if(this.day.equalsIgnoreCase(day)) return true;
        return false;
    }

    public boolean hasTime(String hourOfDay){
        String here = start_hour + ":00 " + start_am_pm;

        if(hourOfDay.equalsIgnoreCase(here)) return true;

        return false;
    }

}
