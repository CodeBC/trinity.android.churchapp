package mcs.church.app.api;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import mcs.church.app.model.Church;

/**
 * Created by h3r0 on 4/24/14.
 */
@EBean
public class ChurchAPI {

    @RootContext
    Context mContext;

    public ArrayList<Church> getAll(){
        final ArrayList<Church> objects = new ArrayList<Church>();

        String url = "http://176.56.237.217/git/church/directory.json";

        Ion.with(mContext).load(url)
                .setLogging(getClass().getSimpleName(), Log.DEBUG)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        Log.i(getClass().getSimpleName(),"Result : " + result);

                        try {

                            if(e != null) throw e;

                            JsonParser parser = new JsonParser();

                            InputStream in = new ByteArrayInputStream(result.getBytes("UTF-8"));

                            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
                            reader.setLenient(true);

                            JsonArray jarr= (JsonArray) parser.parse(reader);

                            Gson gson = new GsonBuilder().create();

                            for(int i=0;i<jarr.size();i++){
                                JsonObject jobj = jarr.get(i).getAsJsonObject();

                                Church obj = gson.fromJson(jobj,Church.class);

                                objects.add(obj);
                            }

                        }catch (Exception ex){
                            ex.printStackTrace();
                        }

                    }
                });

        return objects;
    }

    public ArrayList<Church> SearchByName(String name){
        ArrayList<Church> objects = new ArrayList<Church>();
        return objects;
    }

    public ArrayList<Church> SearchByLocation(String location){
        ArrayList<Church> objects = new ArrayList<Church>();
        return objects;
    }

    public ArrayList<Church> SearchByServiceTime(String date,String time){
        ArrayList<Church> objects = new ArrayList<Church>();
        return objects;
    }
}
