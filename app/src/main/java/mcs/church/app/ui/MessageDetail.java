package mcs.church.app.ui;

import android.app.ActionBar;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Interpolator;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.AnimationRes;

import java.io.ByteArrayOutputStream;
import java.io.File;

import mcs.church.app.R;
import mcs.church.app.core.BaseActivity;
import mcs.church.app.model.ChurchMessage;
import mcs.church.app.util.Blur;
import mcs.church.app.util.ImageUtils;
import mcs.church.app.util.Utils;

/**
 * Created by h3r0 on 5/23/14.
 */
@EActivity
public class MessageDetail extends BaseActivity {

    @Bean
    Utils utils;

    @ViewById
    FrameLayout main;

    @ViewById
    ImageView background;

    @ViewById
    ImageView gospel_message;

    @ViewById
    TextView gospel_message_detail;

    @ViewById
    ProgressBar progressBar;

    @Extra
    ChurchMessage object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setAnimation() {
        super.setAnimation();

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        start_enterAnim = R.anim.activity_open_translate_vertical;
        start_exitAnim = R.anim.activity_close_scale;

        finish_enterAnim = R.anim.activity_open_scale;
        finish_exitAnim = R.anim.activity_close_translate_vertical;
    }

    @Override
    protected int layoutResId() {
        runAnimation = false;

        return R.layout.activity_message_detail;
    }

    @OptionsItem(android.R.id.home)
    void Home() {
        onBackPressed();
    }

    @AfterViews
    void AfterViews() {

        ActionBar actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        actionBar.setTitle("");
        actionBar.hide();

        background.setImageResource(object.getBackgroundImage());

        gospel_message.setBackgroundResource(R.drawable.dark_bg);

        gospel_message_detail.setText(Html.fromHtml(object.detail_link));

        int top_margin = (ImageUtils.getScreenHeight(this) * 20) / 100;

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) gospel_message_detail.getLayoutParams();
        params.setMargins(0, top_margin, 0, 0); //substitute parameters for left, top, right, bottom
        gospel_message_detail.setLayoutParams(params);

        gospel_message.setVisibility(ImageView.GONE);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        Ion.with(gospel_message)
                .animateIn(R.anim.fade_in)
                .load(object.detail_image)
                .setCallback(new FutureCallback<ImageView>() {
                    @Override
                    public void onCompleted(Exception e, ImageView result) {
                        gospel_message.setVisibility(ImageView.VISIBLE);
                        progressBar.setVisibility(ProgressBar.GONE);
                    }
                });

    }

    @Click(R.id.gospel_message)
    void ShowActionBar() {
        Animation bottomUp = AnimationUtils.loadAnimation(this,R.anim.bottom_up);
        gospel_message_detail.setAnimation(bottomUp);
        gospel_message_detail.setVisibility(TextView.VISIBLE);
    }

    @Click(R.id.gospel_message_detail)
    void hideText() {
        Animation bottomDown = AnimationUtils.loadAnimation(this, R.anim.bottom_down);
        gospel_message_detail.setAnimation(bottomDown);
        gospel_message_detail.setVisibility(TextView.GONE);
    }

}
