package mcs.church.app.ui;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import mcs.church.app.R;
import mcs.church.app.core.BaseActivity;
import mcs.church.app.fragment.SearchByLocation;
import mcs.church.app.fragment.SearchByLocation_;
import mcs.church.app.fragment.SearchByName;
import mcs.church.app.fragment.SearchByName_;
import mcs.church.app.fragment.SearchByServiceTiming;
import mcs.church.app.fragment.SearchByServiceTiming_;
import mcs.church.app.util.Constants;
import mcs.church.app.util.Utils;
import mcs.church.app.util.ZoomOutPageTransformer;

/**
 * Created by h3r0 on 5/16/14.
 */
@EActivity
public class Search extends BaseActivity{

    @ViewById
    ViewPager pager;

    @ViewById
    PagerSlidingTabStrip tabs;

    @Bean
    Utils utils;

    @Override
    protected int layoutResId() {
        return R.layout.activity_search;
    }

    @AfterViews
    void AfterView() {
        pager.setAdapter(new SearchFragmentPagerAdapter());
        pager.setPageTransformer(true, new ZoomOutPageTransformer());
        tabs.setViewPager(pager);
        tabs.setTypeface(utils.getTypefaceBlanchCondensed(), android.R.style.TextAppearance_DeviceDefault_Large);
        tabs.setTextSize(40);
        tabs.setTextColor(Color.BLACK);
    }

    @OptionsItem(android.R.id.home)
    void Home() {
        onBackPressed();
    }

    @OptionsItem(R.id.action_bar_share)
    void Share(){
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, Constants.share_appId(this));
        startActivity(Intent.createChooser(intent, "Share via..."));
    }

    public class SearchFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 3;

        public SearchFragmentPagerAdapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    SearchByName searchByName = new SearchByName_();
                    return searchByName;
                case 1:
                    SearchByLocation searchByLocation = new SearchByLocation_();
                    return searchByLocation;
                case 2:
                    SearchByServiceTiming searchByServiceTiming = new SearchByServiceTiming_();
                    return searchByServiceTiming;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Name";
                case 1:
                    return "Location";
                case 2:
                    return "Service Time";

            }
            return "";
        }
    }

}
