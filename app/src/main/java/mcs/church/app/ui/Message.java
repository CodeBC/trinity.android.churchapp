package mcs.church.app.ui;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Window;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.viewpagerindicator.CirclePageIndicator;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import mcs.church.app.R;
import mcs.church.app.adapter.MessageAdapter;
import mcs.church.app.adapter.MessageIndicatorAdapter;
import mcs.church.app.core.BaseActivity;
import mcs.church.app.model.ChurchMessage;
import mcs.church.app.util.Constants;
import mcs.church.app.util.Utils;
import mcs.church.app.util.ZoomOutPageTransformer;

@EActivity
public class Message extends BaseActivity {

    @ViewById
    ViewPager frame;

    @ViewById
    ViewPager indicator;

    @ViewById
    CirclePageIndicator frame_indicator;

    @Bean
    Utils utils;

    @ViewById
    ProgressBar progressBar;

    MessageAdapter adapter;

    MessageIndicatorAdapter adapter_indicator;

    private ArrayList<ChurchMessage> objects = new ArrayList<ChurchMessage>();

    private ArrayList<ChurchMessage> objects_indicator = new ArrayList<ChurchMessage>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int layoutResId() {

        runAnimation = false;

        return R.layout.activity_message;
    }

    @Override
    public void setAnimation() {
        super.setAnimation();
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
    }

    @OptionsItem(android.R.id.home)
    void Home() {
        onBackPressed();
    }

    @OptionsItem(R.id.action_bar_share)
    void Share() {
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, Constants.share_appId(this));
        startActivity(Intent.createChooser(intent, "Share via..."));
    }

    @AfterViews
    void AfterViews() {

        ActionBar actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        actionBar.setTitle("");

        objects = (ArrayList<ChurchMessage>) utils.ReadArrayListFromInternal(Constants.file_gospel_message);
        objects_indicator = (ArrayList<ChurchMessage>) utils.ReadArrayListFromInternal(Constants.file_indicator);

        if (objects.isEmpty()) {
            frame.setVisibility(ListView.GONE);
            progressBar.setVisibility(ProgressBar.VISIBLE);
        } else {
            frame.setVisibility(ListView.VISIBLE);
            progressBar.setVisibility(ProgressBar.GONE);
        }

        adapter = new MessageAdapter(getSupportFragmentManager(), objects);
        adapter_indicator = new MessageIndicatorAdapter(getSupportFragmentManager(), objects_indicator);

        frame.setAdapter(adapter);
        frame.setPageTransformer(true, new ZoomOutPageTransformer());

        indicator.setAdapter(adapter_indicator);

        frame_indicator.setViewPager(indicator);

        frame.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                frame_indicator.setCurrentItem(position / 2);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        String url = "http://mschurch.herokuapp.com/api/messages";

        Ion.with(this).load(url)
                .setLogging(getClass().getSimpleName(), Log.DEBUG)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        Log.i(getClass().getSimpleName(), "Result : " + result);

                        try {

                            if (e != null) throw e;

                            JsonParser parser = new JsonParser();

                            InputStream in = new ByteArrayInputStream(result.getBytes("UTF-8"));

                            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
                            reader.setLenient(true);

                            JsonArray jarr = (JsonArray) parser.parse(reader);

                            Gson gson = new GsonBuilder().create();

                            objects.clear();
                            objects_indicator.clear();

                            for (int i = 0; i < jarr.size(); i++) {
                                JsonObject jobj = jarr.get(i).getAsJsonObject();

                                ChurchMessage obj = gson.fromJson(jobj, ChurchMessage.class);

                                ChurchMessage obj_detail = gson.fromJson(jobj, ChurchMessage.class);
                                obj_detail.title_image = obj.detail_image;

                                obj.id = i * 2 + 1;
                                obj_detail.id = i * 2 + 2;

                                objects.add(obj);
                                objects.add(obj_detail);

                                objects_indicator.add(obj);
                            }

                            utils.SaveArrayListToInternal(Constants.file_gospel_message, objects);
                            utils.SaveArrayListToInternal(Constants.file_indicator, objects_indicator);

                            adapter.setData(objects);
                            adapter_indicator.setData(objects_indicator);

                            frame.setVisibility(ListView.VISIBLE);
                            progressBar.setVisibility(ProgressBar.GONE);

                        } catch (Exception ex) {
                            ex.printStackTrace();

                            if (!objects.isEmpty()) {
                                frame.setVisibility(ListView.VISIBLE);
                                progressBar.setVisibility(ProgressBar.GONE);
                            }
                        }

                    }
                });

    }
}
