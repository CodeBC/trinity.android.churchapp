package mcs.church.app.ui;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import mcs.church.app.R;
import mcs.church.app.core.BaseActivity;
import mcs.church.app.fragment.ChurchDetail;
import mcs.church.app.fragment.ChurchDetail_;
import mcs.church.app.fragment.ChurchServiceTime;
import mcs.church.app.fragment.ChurchServiceTime_;
import mcs.church.app.model.Church;
import mcs.church.app.util.Constants;

/**
 * Created by h3r0 on 5/2/14.
 */
@EActivity
public class DirectoryDetail extends BaseActivity {

    @Extra
    Church church;

    @ViewById
    ViewPager pager;

    @ViewById
    PagerSlidingTabStrip tabs;

    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setTitle(church.name);
    }

    @Override
    protected int layoutResId() {

        runAnimation = false;

        return R.layout.activity_directory_detail;
    }

    @OptionsItem(android.R.id.home)
    void Home() {
        onBackPressed();
    }

    @OptionsItem(R.id.action_bar_share)
    void Share() {
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, Constants.share_appId(this));
        startActivity(Intent.createChooser(intent, "Share via..."));
    }

    @AfterViews
    void AfterView() {

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        map.addMarker(new MarkerOptions().position(new LatLng(church.latitude, church.longitude)));

        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(church.latitude, church.longitude));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);

        map.moveCamera(center);
        map.animateCamera(zoom);

        pager.setAdapter(new SampleFragmentPagerAdapter());
        tabs.setViewPager(pager);
        tabs.setTextColor(Color.BLACK);
    }

    private boolean checkPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
                showErrorDialog(status);
            }
            return false;
        }
        return true;
    }

    void showErrorDialog(int code) {
        GooglePlayServicesUtil.getErrorDialog(code, this, 1001).show();
    }

    //Sample Code
    public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;

        public SampleFragmentPagerAdapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    ChurchDetail churchDetail = new ChurchDetail_();
                    churchDetail.setChurch(church);
                    return churchDetail;
                case 1:
                    ChurchServiceTime fragment = new ChurchServiceTime_();
                    fragment.setChurch(church);
                    return fragment;
            }

            return PageFragment.create(position + 1);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "DETAIL";
                case 1:
                    return "SERVICE TIME";

            }
            return "";
        }
    }

    public static class PageFragment extends Fragment {
        public static final String ARG_PAGE = "ARG_PAGE";

        private int mPage;

        public static PageFragment create(int page) {
            Bundle args = new Bundle();
            args.putInt(ARG_PAGE, page);
            PageFragment fragment = new PageFragment();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mPage = getArguments().getInt(ARG_PAGE);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_church_detail, container, false);
            TextView textView = (TextView) view;
            textView.setText("Fragment #" + mPage);
            return view;
        }
    }
}
