package mcs.church.app.ui;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import mcs.church.app.R;
import mcs.church.app.core.BaseActivity;

@EActivity
public class About extends BaseActivity {

    @ViewById
    ImageView img_about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int layoutResId() {

        runAnimation = false;

        return R.layout.activity_about;
    }

    @Override
    public void setAnimation() {
        super.setAnimation();
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
    }

    @OptionsItem(android.R.id.home)
    void Home() {
        onBackPressed();
    }

    @AfterViews
    void AfterViews() {
        ActionBar actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
