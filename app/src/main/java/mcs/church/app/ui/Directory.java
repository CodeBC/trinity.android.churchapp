package mcs.church.app.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import mcs.church.app.R;
import mcs.church.app.adapter.DirectoryAdapter;
import mcs.church.app.core.BaseActivity;
import mcs.church.app.model.Church;
import mcs.church.app.util.Constants;
import mcs.church.app.util.L;
import mcs.church.app.util.Utils;
import mcs.church.app.view.AnimatedRectLayout;

@EActivity
@OptionsMenu({R.menu.menu_search_light})
public class Directory extends BaseActivity {

    @Bean
    Utils utils;

    @ViewById
    ListView list_directory;

    @ViewById
    ProgressBar progressBar;

    @ViewById
    TextView error_message;

    @Bean
    DirectoryAdapter adapter;

    ArrayList<Church> objects = new ArrayList<Church>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int layoutResId() {

        runAnimation = false;

        return R.layout.activity_directory;
    }

    @OptionsItem(android.R.id.home)
    void Home() {
        onBackPressed();
    }

    @ItemClick(R.id.list_directory)
    void GoToDetail(int position) {
        Intent detail = new Intent(this, DirectoryDetail_.class);
        Bundle args = new Bundle();
        args.putSerializable("church", adapter.getItem(position));
        detail.putExtras(args);
        detail.putExtra("animation_type", AnimatedRectLayout.ANIMATION_RANDOM);
        startActivity(detail);
        overridePendingTransition(0,0);
    }

    @OptionsItem(R.id.action_bar_search)
    void Search(){
        Intent search = new Intent(this, Search_.class);
        search.putExtra("animation_type", AnimatedRectLayout.ANIMATION_RANDOM);
        startActivity(search);
        overridePendingTransition(0, 0);
    }

    @OptionsItem(R.id.action_bar_share)
    void Share(){
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, Constants.share_appId(this));
        startActivity(Intent.createChooser(intent, "Share via..."));
    }

    @AfterViews
    void AfterView() {

        objects = (ArrayList<Church>) utils.ReadArrayListFromInternal(Constants.file_directory);
        adapter.setData(objects);

        L.i("Size : " + objects.size());

        list_directory.setAdapter(adapter);

        list_directory.setVisibility(ListView.GONE);
        progressBar.setVisibility(ProgressBar.VISIBLE);
        error_message.setVisibility(TextView.GONE);

        String url = "http://mschurch.herokuapp.com/api/churches";

        Ion.with(this).load(url)
                .setLogging(getClass().getSimpleName(), Log.DEBUG)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        Log.i(getClass().getSimpleName(), "Result : " + result);

                        try {

                            if (e != null) throw e;

                            JsonParser parser = new JsonParser();

                            InputStream in = new ByteArrayInputStream(result.getBytes("UTF-8"));

                            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
                            reader.setLenient(true);

                            JsonArray jarr = (JsonArray) parser.parse(reader);

                            Gson gson = new GsonBuilder().create();

                            objects.clear();

                            for (int i = 0; i < jarr.size(); i++) {
                                JsonObject jobj = jarr.get(i).getAsJsonObject();

                                Church obj = gson.fromJson(jobj, Church.class);

                                objects.add(obj);
                            }

                            adapter.setData(objects);

                            utils.SaveArrayListToInternal(Constants.file_directory,objects);

                            list_directory.setVisibility(ListView.VISIBLE);
                            progressBar.setVisibility(ProgressBar.GONE);
                            error_message.setVisibility(TextView.GONE);

                        } catch (Exception ex) {
                            ex.printStackTrace();

                            if(!objects.isEmpty()){
                                list_directory.setVisibility(ListView.VISIBLE);
                                progressBar.setVisibility(ProgressBar.GONE);
                                error_message.setVisibility(TextView.GONE);
                            }else{
                                list_directory.setVisibility(ListView.GONE);
                                progressBar.setVisibility(ProgressBar.GONE);
                                error_message.setVisibility(TextView.VISIBLE);
                                error_message.setText("Connection Problem");
                            }
                        }

                    }
                });

    }

}
