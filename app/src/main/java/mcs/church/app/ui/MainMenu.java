package mcs.church.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;

import java.util.Random;

import mcs.church.app.R;
import mcs.church.app.util.Constants;
import mcs.church.app.view.AnimatedRectLayout;

@EActivity(R.layout.activity_main_menu)
@NoTitle
public class MainMenu extends Activity {

    @ViewById
    ImageView share;

    @ViewById
    ImageView first;

    @ViewById
    ImageView second;

    @ViewById
    ImageView third;

    @ViewById
    ImageView fourth;

    @ViewById
    ImageView fifth;

    @ViewById
    ImageView directory;

    @ViewById
    ImageView gospel;

    @ViewById
    ImageView about;

    Animation anim_directory;
    Animation anim_gospel;
    Animation anim_about;

    Animation anim_first, anim_second, anim_third, anim_fourth, anim_fifth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        anim_directory = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_right);
        anim_gospel = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_left);
        anim_about = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_right);

        anim_first = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.first);
        anim_second = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.second);
        anim_third = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.third);
        anim_fourth = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fourth);
        anim_fifth = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fifth);
    }

    @AfterViews
    void AfterViews() {

        Handler dir = new Handler();
        dir.postDelayed(new Runnable() {
            @Override
            public void run() {
                directory.setVisibility(ImageView.VISIBLE);
                directory.startAnimation(anim_directory);
            }
        }, 300);

        Handler gosp = new Handler();
        gosp.postDelayed(new Runnable() {
            @Override
            public void run() {
                gospel.setVisibility(ImageView.VISIBLE);
                gospel.startAnimation(anim_gospel);
            }
        }, 900);

        Handler abu = new Handler();
        abu.postDelayed(new Runnable() {
            @Override
            public void run() {
                about.setVisibility(ImageView.VISIBLE);
                about.startAnimation(anim_about);
            }
        }, 600);

        first.setImageBitmap(generateBitmap());
        second.setImageBitmap(generateBitmap());
        third.setImageBitmap(generateBitmap());
        fourth.setImageBitmap(generateBitmap());
        fifth.setImageBitmap(generateBitmap());

        first.startAnimation(anim_first);
        second.startAnimation(anim_second);
        third.startAnimation(anim_third);
        fourth.startAnimation(anim_fourth);
        fifth.startAnimation(anim_fifth);

        anim_first.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                first.startAnimation(anim_first);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        anim_second.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                second.startAnimation(anim_second);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        anim_third.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                third.startAnimation(anim_third);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        anim_fourth.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fourth.startAnimation(anim_fourth);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        anim_fifth.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fifth.startAnimation(anim_fifth);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Click(R.id.directory)
    void GoToDirectory() {
        Intent directory = new Intent(this, Directory_.class);
        directory.putExtra("animation_type", AnimatedRectLayout.ANIMATION_RANDOM);
        startActivity(directory);
        overridePendingTransition(0, 0);
    }

    @Click(R.id.gospel)
    void GoToGospel() {
        Intent message = new Intent(this, Message_.class);
        startActivity(message);
        overridePendingTransition(0, 0);
    }

    @Click(R.id.about)
    void GoToAbout() {
        Intent about = new Intent(this, About_.class);
        about.putExtra("animation_type", AnimatedRectLayout.ANIMATION_RANDOM);
        startActivity(about);
        overridePendingTransition(0, 0);
    }

    private Bitmap generateBitmap() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.ADD));
        Random rnd = new Random();
        int color = Color.argb(50, 255, 255, 255);
        paint.setColor(color);
        Bitmap bg = Bitmap.createBitmap(480, 800, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bg);
        canvas.drawCircle(200, 200, 200, paint);

        return bg;
    }

    @Click(R.id.share)
    void Share() {
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, Constants.share_appId(this));
        startActivity(Intent.createChooser(intent, "Share via..."));
    }
}
