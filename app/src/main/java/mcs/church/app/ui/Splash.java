package mcs.church.app.ui;

import android.app.Activity;
import android.content.Intent;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.NoTitle;

import mcs.church.app.R;

@EActivity(R.layout.activity_splash)
@Fullscreen
@NoTitle
public class Splash extends Activity {

    @AfterViews
    public void calledAfterViewInjection() {

        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Intent main = new Intent(Splash.this, MainMenu_.class);
                    startActivity(main);

                    finish();

                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }
        };

        splashTread.start();

    }
}
