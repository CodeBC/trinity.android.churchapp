package mcs.church.app.util;

import android.content.Context;

/**
 * Created by h3r0 on 5/15/14.
 */
public class Constants {

    public static String file_directory = "directory.dat";

    public static String file_gospel_message = "message.dat";

    public static String file_indicator = "indicator.dat";

    public static String share_appId(Context mContext) {
        return "Hi, I have a wonderful app to share with you: " +
                "https://play.google.com/store/apps/details?id=" +
                mContext.getPackageName();
    }
}
