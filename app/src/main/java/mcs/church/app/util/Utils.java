package mcs.church.app.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Collection of Utility functions
 *
 * @author Hein Win Toe (hein[at]nexlabs[dot]co)
 * @since 1.0.0
 */

@EBean
public class Utils {

    @RootContext
    Context mContext;

    public Utils() {
        // TODO Auto-generated constructor stub
    }

    /*
     * Retrun Blanch Condensed Typeface
     *
     * @return Typeface for Zawgyi font
     */
    public Typeface getTypefaceBlanchCondensed() {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "BLANCH_CONDENSED.ttf");
        return tf;
    }

    /**
     * Return Device Unique ID
     *
     * @return Device UDID
     */
    public String udid() {
        String udid = android.provider.Settings.System.getString(mContext.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        return udid;
    }

    /**
     * Call Phone No.
     *
     * @param Phone Number
     * @return
     */
    public void PhoneCall(String number) {
        Intent call = new Intent(Intent.ACTION_DIAL);
        call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        call.setData(Uri.parse("tel:" + number));
        mContext.startActivity(call);
    }

    /**
     * Send Email
     *
     * @param Email
     * @return
     */
    public void SendEmail(String mail) {
        Intent email = new Intent(Intent.ACTION_SEND);
        email.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        email.setType("text/plain");
        email.putExtra(Intent.EXTRA_EMAIL, mail);
        email.putExtra(Intent.EXTRA_SUBJECT, "");
        email.putExtra(Intent.EXTRA_TEXT, "");

        mContext.startActivity(Intent.createChooser(email, "Send Email"));
    }

    /**
     * Open URL in Browser
     *
     * @param Web URL
     * @return
     */
    public void OpenBrowser(String link) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        mContext.startActivity(browserIntent);
    }

    /**
     * Read Text from assets folder and return String
     *
     * @param The name of the file to open (under assets folder)
     * @return String value of requested file
     */
    public String loadTextFromAssets(String filename) {
        try {
            InputStream is = mContext.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Open a private file associated with this Context's application package for reading.
     *
     * @param The name of the file to open
     * @return Object
     */
    public Object ReadArrayListFromInternal(String filename) {
        try {
            FileInputStream fis = mContext.openFileInput(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object obj = (Object) ois.readObject();
            fis.close();
            return obj;

        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<Object>();
        }
    }

    /**
     * Open a private file associated with this Context's application package for writing. Creates the file if it doesn't already exist.
     *
     * @param The    name of the file to write
     * @param String data to write
     * @return
     */
    public <E> void SaveArrayListToInternal(String filename, ArrayList<E> list) {
        try {

            L.i(mContext.getFileStreamPath(filename).getAbsolutePath());

            FileOutputStream fos = mContext.openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
