package mcs.church.app.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import mcs.church.app.api.ChurchAPI;
import mcs.church.app.model.Church;
import mcs.church.app.view.DirectoryItemView;
import mcs.church.app.view.DirectoryItemView_;

/**
 * Created by h3r0 on 4/24/14.
 */
@EBean
public class DirectoryAdapter extends BaseAdapter {

    @Bean(ChurchAPI.class)
    ChurchAPI api;

    @RootContext
    Context mContext;

    ArrayList<Church> objects = new ArrayList<Church>();

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Church getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return objects.get(position).id;
    }

    public void setData(ArrayList<Church> objects){

        this.objects = objects;
        Collections.sort(this.objects,new Comparator<Church>() {
            @Override
            public int compare(Church lhs, Church rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });

        notifyDataSetChanged();
    }

    public void setData(ArrayList<Church> objects,boolean sort){

        if(sort){
            setData(objects);
        }else{
            this.objects = objects;
            notifyDataSetChanged();
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DirectoryItemView view;

        if (convertView == null) {
            view = DirectoryItemView_.build(mContext);
        } else {
            view = (DirectoryItemView) convertView;
        }

        view.bind(getItem(position));

        return view;
    }
}
