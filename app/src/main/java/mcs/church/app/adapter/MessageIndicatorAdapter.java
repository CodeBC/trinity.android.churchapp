package mcs.church.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import mcs.church.app.model.ChurchMessage;
import mcs.church.app.util.L;

/**
 * Created by h3r0 on 5/14/14.
 */
public class MessageIndicatorAdapter extends FragmentStatePagerAdapter {

    private ArrayList<ChurchMessage> objects = new ArrayList<ChurchMessage>();

    public MessageIndicatorAdapter(FragmentManager fm, ArrayList<ChurchMessage> objects) {
        super(fm);

        this.objects = objects;
    }

    public void setData(ArrayList<ChurchMessage> objects){
        this.objects = objects;

        Collections.sort(objects, new Comparator<ChurchMessage>() {
            @Override
            public int compare(ChurchMessage lhs, ChurchMessage rhs) {
                return lhs.order_number - rhs.order_number;
            }
        });

        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {

        L.i("Indicator Position : " + position);

        Fragment frag = new Fragment();
        return frag;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

}
