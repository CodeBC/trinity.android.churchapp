package mcs.church.app.core;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Property;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import mcs.church.app.R;
import mcs.church.app.view.AnimatedRectLayout;

/**
 * Created by h3r0 on 5/6/14.
 */
public abstract class BaseActivity extends FragmentActivity {

    Typeface blanch_condensed;

    private AnimatedRectLayout mAnimated;
    protected int mAnimationType;

    public boolean runAnimation = true;

    public int start_enterAnim;
    public int start_exitAnim;
    public int finish_enterAnim;
    public int finish_exitAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setAnimation();

        overridePendingTransition(start_enterAnim, start_exitAnim);

        blanch_condensed = Typeface.createFromAsset(getAssets(), "BLANCH_CONDENSED.ttf");

        getActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        int actionBarTitleId = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
        if (actionBarTitleId > 0) {
            TextView title = (TextView) findViewById(actionBarTitleId);
            if (title != null) {
                title.setTextColor(Color.parseColor("#51c5d4"));
                title.setTypeface(blanch_condensed);
                title.setTextSize(25);
            }
        }

        setContentView(layoutResId());

        if (runAnimation) {
            FrameLayout activityRoot = (FrameLayout) findViewById(android.R.id.content);
            View parent = activityRoot.getChildAt(0);

            mAnimated = new AnimatedRectLayout(this);
            activityRoot.removeView(parent);
            activityRoot.addView(mAnimated, parent.getLayoutParams());
            mAnimated.addView(parent);

            mAnimationType = getIntent().getIntExtra("animation_type", AnimatedRectLayout.ANIMATION_RANDOM);
            mAnimated.setAnimationType(mAnimationType);

            ObjectAnimator animator = ObjectAnimator.ofFloat(mAnimated, ANIMATED_RECT_LAYOUT_FLOAT_PROPERTY, 1).setDuration(1000);
            animator.start();
        }
    }

    public void setAnimation() {
        start_enterAnim = R.anim.activity_open_translate;
        start_exitAnim = R.anim.activity_close_scale;

        finish_enterAnim = R.anim.activity_open_scale;
        finish_exitAnim = R.anim.activity_close_translate;
    }

    protected abstract int layoutResId();

    @Override
    public void onBackPressed() {

        if (runAnimation) {
            ObjectAnimator animator = ObjectAnimator.ofFloat(mAnimated, ANIMATED_RECT_LAYOUT_FLOAT_PROPERTY, 0).setDuration(1000);
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    finish();
                }
            });
            animator.start();
        } else {
            finish();
        }
    }


    private static final Property<AnimatedRectLayout, Float> ANIMATED_RECT_LAYOUT_FLOAT_PROPERTY =
            new Property<AnimatedRectLayout, Float>(Float.class, "ANIMATED_RECT_LAYOUT_FLOAT_PROPERTY") {

                @Override
                public void set(AnimatedRectLayout layout, Float value) {
                    layout.setProgress(value);
                }

                @Override
                public Float get(AnimatedRectLayout layout) {
                    return layout.getProgress();
                }
            };

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(finish_enterAnim, finish_exitAnim);
    }
}
